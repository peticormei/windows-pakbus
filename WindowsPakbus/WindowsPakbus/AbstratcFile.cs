﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Text;
using System.Threading.Tasks;

namespace WindowsPakbus
{
    class AbstratcFile
    {
        private string name;
        private string extention;
        private string content;

        public AbstratcFile(string name, string extention, string content)
        {
            this.name = name;
            this.extention = extention;
            this.content = AbstratcFile.HexStringToString(content);
        }

        public string saveFile()
        {
            string fileName = name + extention;

            var myFile = File.Create(fileName);
            using (FileStream fs = myFile)
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(content);
                fs.Write(info, 0, info.Length);
            }
            myFile.Close();

            return Path.GetFullPath(fileName);
        }

        public static string StringToHexString(string file, string extention)
        {
            var str_hex = new SoapHexBinary(File.ReadAllBytes(file + extention)).ToString();
            return str_hex;
        }

        public static string HexStringToString(string content)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i <= content.Length - 2; i += 2)
            {
                sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(content.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
            }

            return sb.ToString();
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public string getName()
        {
            return name;
        }

        public void setExtention(string extention)
        {
            this.extention = extention;
        }

        public string getExtention()
        {
            return extention;
        }

        public void setContent(string content)
        {
            this.content = content;
        }

        public string getContent()
        {
            return content;
        }
    }
}
