﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace WindowsPakbus
{
    class NorthAPI
    {
        MqttConection mqtt;

        private static NorthAPI north;
        private Device device = Device.getInstance();

        public NorthAPI() { }

        public void reciveJson(MqttMsgPublishEventArgs e)
        {
            var jsonText = Encoding.UTF8.GetString(e.Message);
            var json = (JObject)JsonConvert.DeserializeObject(jsonText);

            if (e.Topic == "request")
            {
                if ((string)json["method"] == "config")
                {
                    ConfigDatalogger(json);
                }
                else if ((string)json["method"] == "program")
                {
                    ProgramDatalogger(json);
                }
            }
            else if (e.Topic == "call")
            {
                if ((string)json["method"] == "clock")
                {
                    Clock();
                }
                else if ((string)json["method"] == "get_tables")
                {
                    GetTables();
                }
                /*
                else if ((string)json["method"] == "get_table_header")
                {
                    GetTableHeader();
                }
                else if ((string)json["method"] == "get_table_data")
                {
                    GetTableData();
                }
                */
                else if ((string)json["method"] == "get_table_count")
                {
                    GetTableCount();
                }
                else if ((string)json["method"] == "table")
                {
                    Table();
                }
            }
        }

        private void Table()
        {
            ///mqtt.publish("request", string.Join(",", device.Table().ToArray()));
            mqtt.publish("response", mqtt.convertListToJson(device.Table()));
        }

        public void requestConfig()
        {
            mqtt.publish("request", "request_config");
        }

        private void Clock()
        {
            mqtt.publish("return", device.getClock());
        }

        private void GetTables()
        {
            mqtt.publish("return", device.GetTables());
        }
        /*
        private void GetTableHeader()
        {
            mqtt.publish("return", device.GetHeader(5));
        }

        private void GetTableData()
        {
            mqtt.publish("return", device.GetData(5, 0));
        }
        */
        private void GetTableCount()
        {
            mqtt.publish("return", device.GetTableCount(5));
        }

        private void ProgramDatalogger(JObject json)
        {
            AbstratcFile file = JsonConvert.DeserializeObject<AbstratcFile>(json["file"].ToString());
            string pathToFile = file.saveFile();
            device.setProgram(pathToFile);
        }

        private void ConfigDatalogger(JObject json)
        {
            var myFile = File.Create("config.json");
            using (FileStream fs = myFile)
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(json["param"].ToString());
                fs.Write(info, 0, info.Length);
            }
            myFile.Close();
            //Datalogger datalogger = JsonConvert.DeserializeObject<Datalogger>(json["param"].ToString());
        }

        public Datalogger ConfigDatalogger(string json)
        {
            Datalogger datalogger = JsonConvert.DeserializeObject<Datalogger>(json);
            return datalogger;
            /*
            { using (StreamReader r = new StreamReader(json))
                {
                    string jsonText = r.ReadToEnd();
                    Datalogger datalogger = JsonConvert.DeserializeObject<Datalogger>(jsonText);
                    return datalogger;
                }
            }
            */
        }

        public void mqttConection(string adress)
        {
            mqtt = new MqttConection(adress);
        }

        public void mqttSubscribe(string topic)
        {
            mqtt.subscribe(topic);
        }

        public static NorthAPI getInstance()
        {
            if (north is null)
            {
                north = new NorthAPI();
            }
            return north;
        }

    }
}
