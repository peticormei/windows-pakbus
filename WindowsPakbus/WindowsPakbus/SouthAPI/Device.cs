﻿using SmplPB_CS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

namespace WindowsPakbus
{
    class Device
    {
        private static Device device;

        public int returnval;
        private Datalogger datalogger;

        public Device() { }

        public void setDatalogger(Datalogger datalogger)
        {
            this.datalogger = datalogger;
        }

        public Datalogger getDatalogger()
        {
            return datalogger;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public int openPort()
        {
            returnval = SimplePB.OpenPort(datalogger.getComPort(), datalogger.getBaudRate());
            return returnval;
            /*
            if (returnval == 0)
                return "COM port opened";
            else
                //return "COM port failed to open";
            */
        }
        
        public string closePort()
        {
            returnval = SimplePB.ClosePort();
            
            if (returnval == 0)
                return "Port Closed";
            else
                return "Error closing port";
        }

        public string getClock()
        {
            ClockReturn returnval;
            string strClock = String.Empty;
            
            returnval = SimplePB.GetClock(datalogger.getPakbus(), datalogger.getDevice(), ref strClock);
            
            if (returnval == 0)
                return ("Clock Length: " + strClock.Length.ToString() + " Clock Value: " + strClock);
            else
                return ("Clock Check error: " + returnval.ToString());

        }

        public string setClock()
        {
            ClockReturn returnval;
            string strClock = String.Empty;

            returnval = SimplePB.SetClock(datalogger.getPakbus(), datalogger.getDevice(), ref strClock);

            //Display Results
            if (returnval == 0)
                return ("Clock Length: " + strClock.Length.ToString() + " Clock Value: " + strClock );
            else
                return ("Clock Set error: " + returnval.ToString());

        }

        public string getStatus()
        {
            GetStatusReturn returnval;
            string strStatus = String.Empty;

            returnval = SimplePB.GetStatus(datalogger.getPakbus(), datalogger.getDevice(), ref strStatus);

            //Display Results
            if (returnval == 0)
                return ("Status of " + datalogger.getDevice().ToString() + ": \r\n" + strStatus);
            else
                return ("Get Status error: " + returnval.ToString());

        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public String GetTables() // ARRUMAR COMO O DADO É EXIBIDO (TALVEZ RETORNAR UM JSON)
        {
            GetTablesReturn returnval;
            string strTables = String.Empty;

            returnval = SimplePB.GetTableNames(datalogger.getPakbus(), datalogger.getDevice(), ref strTables);

            if (returnval == 0)
            {
                //string correctStrTables = strTables.Replace("\r\n"," ");
                //string[] array = correctStrTables.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                //return array[0].ToString();
                string replacement = Regex.Replace(strTables, @"[0-9]|\r|\n", string.Empty).Trim().Replace(" ", ",");
                return replacement;
            }
            else
            {
                return returnval.ToString();
                //return ("Get Tables error: " + returnval.ToString());
            }
        }
        
        public String GetHeader(int tableID)
        {

            GetDataReturn returnval;
            string strHeader = String.Empty;

            returnval = SimplePB.GetDataHeader(datalogger.getPakbus(), datalogger.getDevice(), tableID, ref strHeader);

            if (returnval == 0)
            {
                string replacement = Regex.Replace(strHeader, @"\""", string.Empty).Trim().Replace("\r\n", ",");
                string[] options = replacement.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                List<String> list = options.ToList();
                options = list.GetRange(2, list.IndexOf(list.Last())-3).ToArray();

                replacement = string.Join(",", options);

                return replacement;
            }
            else
            {
                return returnval.ToString();
            }
        }

        public string GetTableCount(int tableID)
        {

            GetDataReturn returnval;
            UInt32 uintRecordsCount = 0;

            returnval = SimplePB.GetTableRecordsCount(datalogger.getPakbus(), datalogger.getDevice(), tableID, ref uintRecordsCount);

            if (returnval == 0)
               return uintRecordsCount.ToString();
            else
                return returnval.ToString();
        }

        public string GetData(int tableID, int recordNum)
        {

            GetDataReturn returnval;
            string strData = String.Empty;
            ArrayList data = new ArrayList();

            returnval = SimplePB.GetCommaData(datalogger.getPakbus(), datalogger.getDevice(), tableID, recordNum, ref strData);

            if (returnval > 0)
            {
                strData = Regex.Replace(strData, @"\""|\r|\n", string.Empty);
                data.Add(strData);

                do
                {
                    returnval = SimplePB.GetCommaData(datalogger.getPakbus(), datalogger.getDevice(), tableID, recordNum, ref strData);

                    if (returnval < 0)
                    {
                        return ("Get Data error: " + returnval.ToString());
                        break;
                    }
                    else
                        strData = Regex.Replace(strData, @"\""|\r|\n", string.Empty);
                        data.Add(strData);
                } while (returnval > 0);
            }
            else if (returnval == 0)
            {
                strData = Regex.Replace(strData, @"\""|\r|\n", string.Empty);
                data.Add(strData);
            }
            else
            {
                return ("Get Data error: " + returnval.ToString());
            }
            string[] myArray = (string[])data.ToArray(typeof(string));
            String str = string.Join(";", myArray);
            return str;
        }

        public List<string> Table()
        {
            string header = GetHeader(5);
            string[] options = header.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            List<String> header_list = options.ToList();

            string data = GetData(5, 0);
            string[] data_list = data.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            List<String> data_list_w = data_list.ToList();

            string file_name = header_list[0];
            header_list.RemoveAt(0);

            string header_string = string.Join(",", header_list.ToArray());

            var myFile = File.Create(file_name+".csv");
            using (FileStream fs = myFile)
            {
                Byte[] info = new UTF8Encoding(true).GetBytes(header_string+",\n");
                fs.Write(info, 0, info.Length);
                foreach (var values in data_list_w)
                {
                    Byte[] info2 = new UTF8Encoding(true).GetBytes(values + ",\n");
                    fs.Write(info2, 0, info2.Length);
                }
            }
            myFile.Close();

            var str_hex = new SoapHexBinary(File.ReadAllBytes(file_name + ".csv")).ToString();

            List<string> list = new List<string>();
            list.Add(file_name);
            list.Add(str_hex);
            list.Add(".csv");

            return list;
        }
        
        public void setProgram(string path)
        {
            FileSendReturn returnval;
            string strData = String.Empty;

            returnval = SimplePB.FileSend(datalogger.getPakbus(), datalogger.getDevice(), path, ref strData);
            
            if (returnval == 0)
                Console.WriteLine(returnval + ", " + strData + "\r\n");
            else if (returnval > 0)
            {
                Console.WriteLine(returnval + "\r\n");

                do
                {
                    returnval = SimplePB.FileSend(datalogger.getPakbus(), datalogger.getDevice(), path, ref strData);

                    if (returnval < 0)
                    {
                        Console.WriteLine("File Send error: " + returnval.ToString() + "\r\n");
                        break;
                    }
                    else
                        Console.WriteLine(returnval + "\r\n");
                } while (returnval > 0);
                if (returnval == 0) Console.WriteLine(strData + "\r\n");
            }
            else
                Console.WriteLine("File Send error: " + returnval.ToString() + "\r\n");
        }

        private string HexString2Ascii(string hexString)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i <= hexString.Length - 2; i += 2)
            {
                sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(hexString.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
            }
            return sb.ToString();
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static Device getInstance()
        {
            if (device is null)
            {
                device = new Device();
            }
            return device;
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
