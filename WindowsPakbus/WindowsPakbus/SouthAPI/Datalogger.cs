﻿using SmplPB_CS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsPakbus
{
    class Datalogger
    {
        private int comPort;
        private int baudRate;
        private int pakbus;
        private DeviceTypeCodes device;

        public Datalogger(int comPort, int baudRate, int device, int pakbus)
        {
            this.comPort = comPort;
            this.baudRate = baudRate;
            this.device = (DeviceTypeCodes)device;
            this.pakbus = pakbus;
        }

        public void setComPort(int comPort)
        {
            this.comPort = comPort;
        }

        public int getComPort()
        {
            return comPort;
        }

        public void setBaudRate(int baudRate)
        {
            this.baudRate = baudRate;
        }

        public int getBaudRate()
        {
            return baudRate;
        }

        public void setPakbus(int pakbus)
        {
            this.pakbus = pakbus;
        }

        public int getPakbus()
        {
            return pakbus;
        }

        public void setDevice(int device)
        {
            this.device = (DeviceTypeCodes)device;
        }

        public DeviceTypeCodes getDevice()
        {
            return device;
        }
    }
}
