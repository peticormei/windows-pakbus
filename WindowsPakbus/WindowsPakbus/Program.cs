﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WindowsPakbus
{
    class Program
    {
        private static string json = "config.json";
        private static DateTime? datetime;
        //private static DateTime datetime = new DateTime();
        private static Device device = Device.getInstance();
        private static NorthAPI north = NorthAPI.getInstance();

        static void Main(string[] args)
        {
            // configuração da conexão com o mosquitto
            north.mqttConection("172.16.189.129");
            north.mqttSubscribe("request");
            north.mqttSubscribe("call");

            Boolean com = false;

            while (com == false)
            {
                if (File.Exists(json))
                {
                    
                    if (datetime == null)
                    {
                        datetime = File.GetLastWriteTime(json);
                        device.setDatalogger(north.ConfigDatalogger(File.ReadAllText(json)));
                    }
                    else
                    {
                        int result = DateTime.Compare((DateTime)datetime, File.GetLastWriteTime(json));
                        if (result < 0)
                        {
                            datetime = File.GetLastWriteTime(json);
                            device.setDatalogger(north.ConfigDatalogger(File.ReadAllText(json)));
                        }
                    }

                    int r = device.openPort();

                    if (r == 0)
                    {
                        com = true;
                    }
                }
                else
                {
                    north.requestConfig();
                }
            }

            Console.WriteLine(device.getDatalogger().getDevice());
            Console.WriteLine(device.getDatalogger().getPakbus());
            Console.WriteLine(device.getDatalogger().getBaudRate());
            Console.WriteLine(device.getDatalogger().getComPort());

            //Console.WriteLine(device.closePort());
            Console.ReadLine();
               
        }
    }
}
