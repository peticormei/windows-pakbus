﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace WindowsPakbus
{
    class MqttConection
    {
        MqttClient client;
        NorthAPI north = NorthAPI.getInstance();

        public MqttConection(string BrokerAddress)
        {
            client = new MqttClient(BrokerAddress);
            client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
            client.Connect(Guid.NewGuid().ToString());
        }

        private void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            /*
            var jsonText = Encoding.UTF8.GetString(e.Message);
            Console.WriteLine("Received = " + jsonText + " on topic " + e.Topic);

            var json = (JObject)JsonConvert.DeserializeObject(jsonText);
            Type type = Assembly.GetExecutingAssembly()
                    .GetTypes()
                    .First(t => t.Name == (string)json["class"]);

            object inst = Activator.CreateInstance(type);
            var method = type.GetMethod((string)json["method"]);
            var parameters = method.GetParameters()
                    .Select(p => Convert.ChangeType((string)json["parameters"][p.Name], p.ParameterType))
                    .ToArray();
            var result = method.Invoke(inst, parameters);
            
            var toReturn = JsonConvert.SerializeObject(new { status = "OK", result = result }).ToString();
            client.Publish("result", Encoding.UTF8.GetBytes(toReturn));
            Console.WriteLine(toReturn);
            */
            north.reciveJson(e);
        }

        public ushort subscribe(string topic)
        {
            ushort toReturn = client.Subscribe(new string[] { topic }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
            return toReturn;
        }

        public string convertListToJson(List<string> list)
        {
            string str = JsonConvert.SerializeObject(new { file_name = list[0], content = list[1], file_extention = list[2]}).ToString();
            return str;
        }

        public void publish(string topic, string result)
        {
            var toReturn = JsonConvert.SerializeObject(new { result = result }).ToString();
            client.Publish(topic, // topic
                              Encoding.UTF8.GetBytes(toReturn), // message body
                              MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, // QoS level
                              false); // retained
        }
    }
}
